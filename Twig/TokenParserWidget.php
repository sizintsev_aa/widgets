<?php
namespace Modules\Widgets\Twig;

class TokenParserWidget extends \Twig_TokenParser
{
    public function parse(\Twig_Token $token)
    {
        $lineno = $token->getLine();
        $stream = $this->parser->getStream();

        list($name, $params) = $this->getInlineParams($token);


        $content = $this->parser->subparse([$this, 'decideBlockEnd'], true);
        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return new TwigNodeWidget($content, $name, $params, $lineno, $this->getTag());
    }

    protected function getInlineParams(\Twig_Token $token)
    {
        $stream = $this->parser->getStream();

        $widgetName = null;
        if ($stream->nextIf(\Twig_Token::OPERATOR_TYPE, 'in')) {
            $widgetName = $this->parser->getExpressionParser()->parseExpression();
        } else {
            $lineno = $token->getLine();
            $widgetName = new \Twig_Node_Expression_Constant('div', $lineno);
        }
        if ($stream->nextIf(\Twig_Token::NAME_TYPE, 'with')) {
            $params = $this->parser->getExpressionParser()->parseExpression();
        } else {
            $lineno = $token->getLine();
            $params = new \Twig_Node_Expression_Array([], $lineno);
        }

        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return [$widgetName, $params];
    }

    public function decideBlockEnd(\Twig_Token $token)
    {
        return $token->test('endwidget');
    }

    public function getTag()
    {
        return 'widget';
    }
}