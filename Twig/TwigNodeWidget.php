<?php
namespace Modules\Widgets\Twig;

class TwigNodeWidget extends \Twig_Node implements \Twig_NodeOutputInterface
{
    protected $tagName = 'widget';

    public function __construct(\Twig_NodeInterface $body = null, $name, $params = [], $lineno, $tag = null)
    {
        parent::__construct(['body' => $body, 'name' => $name, 'params' => $params], [], $lineno, $tag);
    }
    /**
     * Compiles the node to PHP.
     *
     * @param \Twig_Compiler $compiler A Twig_Compiler instance
     */
    public function compile(\Twig_Compiler $compiler)
    {
        $compiler->addDebugInfo($this)
            ->write('$name = ')
            ->subcompile($this->getNode('name'))
            ->raw(";\n")
            ->write('$params = ')
            ->subcompile($this->getNode('params'))
            ->raw(";\n")
            ->write("ob_start();\n")
            ->subcompile($this->getNode('body'))
            ->raw(";\n")
            ->write("\$content = ob_get_clean();\n")
            ->write('echo (new \Modules\Widgets\Entities\FromTwig($name, $params, $content));')
            ->raw("\n");

        /*$compiler->addDebugInfo($this)
            ->write("\$assetFunction = \$this->env->getFunction('parse_assets')->getCallable();\n")
            ->write('$assetVariables = ')
            ->subcompile($this->getNode('variables'))
            ->raw(";\n")
            ->write("if (\$assetVariables && !is_array(\$assetVariables)) {\n")
            ->indent()
            ->write("throw new UnexpectedValueException('{% {$this->tagName} with x %}: x is not an array');\n")
            ->outdent()
            ->write("}\n")
            ->write('$location = ')
            ->subcompile($this->getNode('location'))
            ->raw(";\n")
            ->write("if (\$location && !is_string(\$location)) {\n")
            ->indent()
            ->write("throw new UnexpectedValueException('{% {$this->tagName} in x %}: x is not a string');\n")
            ->outdent()
            ->write("}\n")
            ->write("\$priority = isset(\$assetVariables['priority']) ? \$assetVariables['priority'] : 0;\n")
            ->write("ob_start();\n")
            ->subcompile($this->getNode('body'))
            ->write("\$content = ob_get_clean();\n")
            ->write("\$assetFunction(\$content, \$location, \$priority);\n");*/
    }
}