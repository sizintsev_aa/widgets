<?php
Route::group([
    'prefix'     => 'widgets',
    'middleware' => \Modules\Session\Http\Middleware\SessionMiddleware::class,
], function() {
    Route::post('upload', [
        'as'   => 'components.image.upload',
        'uses' => function(\Illuminate\Http\Request $request){
            $cdn = new \Modules\Cdn\Entities\CDN($request->file('file'));
            try {
                $result  = $cdn->upload();
                return response()->json($result);
            }
            catch (\Modules\Cdn\Entities\CDN\FilesException $e) {
                return response()->json([
                    'status'  => \Modules\Cdn\Entities\CDN::STATUS_ERROR,
                    'message' => $e->getMessage()
                ]);
            }
        }
    ]);
    Route::post('file/upload', [
        'as'   => 'components.file.upload',
        'uses' => function(\Illuminate\Http\Request $request){
            $cdn = new \Modules\Cdn\Entities\CDN($request->file('file'));
            try {
                $result  = $cdn->upload();
                return response()->json($result);
            }
            catch (\Modules\Cdn\Entities\CDN\FilesException $e) {
                return response()->json([
                    'status'  => \Modules\Cdn\Entities\CDN::STATUS_ERROR,
                    'message' => $e->getMessage()
                ]);
            }
        }
    ]);

    Route::post('remove', [
        'as'   => 'components.image.remove',
        'uses' => function(\Illuminate\Http\Request $request){
            return response()->json([
                'status'  => 200,
                'message' => ''
            ]);
        }
    ]);

    Route::get('og', [
        'as'   => 'components.og',
        'uses' => function(\Illuminate\Http\Request $request, Twig_Environment $twig){
            $embed = \Embed\Embed::create($request->input('url'));

            return $twig->render('widgets::widgets/og.twig', [
                'embed' => $embed
            ]);
        }
    ]);

    Route::post('add', [
        'as'   => 'components.add',
        function(\Illuminate\Http\Request $request, Twig_Environment $twig){
            $twig ->addGlobal('name', $request->input('name'));

            switch ($request->input('component')) {
                case 'heading':
                    return $twig->render('widgets::widgets/components/component/heading.twig');
                    break;

                case 'text':
                    return $twig->render('widgets::widgets/components/component/text.twig');
                    break;

                case 'image':
                    return $twig->render('widgets::widgets/components/component/image.twig');
                    break;

                case 'gallery':
                    return $twig->render('widgets::widgets/components/component/gallery.twig');
                    break;

                case 'files':
                    return $twig->render('widgets::widgets/components/component/files.twig');
                    break;

                case 'quote':
                    return $twig->render('widgets::widgets/components/component/quote.twig');
                    break;

                case 'video':
                    return $twig->render('widgets::widgets/components/component/video.twig');
                    break;

                case 'blocks':
                    return $twig->render('widgets::widgets/components/component/blocks.twig');
                    break;
            }

            return '';
        }
    ]);
});
