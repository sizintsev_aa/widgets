<?php
namespace Modules\Widgets\Entities;

class FromTwig
{
    private $widget  = null;
    private $args    = null;
    private $content = null;
    private $class   = null;

    public function __construct($widget, $args, $content)
    {
        $this->widget  = $widget;
        $this->args    = $args;
        $this->content = $content;

        try {
            $class = 'Modules\\Widgets\\Entities\\' . ucfirst(camel_case($this->widget));
            $this->class = new $class;

            foreach ($this->args as $method => $value) {
                if (method_exists($this->class, $method)) {
                    $this->class->{$method}($value);
                }
                else {
                    $this->class->__call($method, [$value]);
                }
            }

            $this->class->setContent($this->content);
        }
        catch (\Exception $e) {
            echo ('Widget error ' . $widget . "\n" . $e->getMessage());
        }
    }

    public function __toString()
    {
        return $this->class->render();
    }
}