<?php
namespace Modules\Widgets\Entities;

class Radio extends WidgetAbstract
{
    protected $_template = 'widgets/radio.twig';
    private $_options = [];

    public function getOptions() : array
    {
        return $this->_options;
    }

    public function addOption(string $value, string $label, array $settings = []) : self
    {
        $this->_options[] = [
            'type'     => 'text',
            'value'    => $value,
            'label'    => $label,
            'settings' => $settings
        ];

        return $this;
    }
}