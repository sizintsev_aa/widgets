<?php
namespace Modules\Widgets\Entities;

class Textarea extends WidgetAbstract
{
    protected $_template = 'widgets/textarea.twig';

    public function __construct()
    {
        $this->setClass('form-control');
    }
}