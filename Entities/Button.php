<?php
namespace Modules\Widgets\Entities;

class Button extends Block
{
    protected $_tag = 'button';

    public function __construct($content = null)
    {
        $this->setClass('btn');

        parent::__construct($content);
    }
}