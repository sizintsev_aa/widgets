<?php
namespace Modules\Widgets\Entities;


class Tree extends WidgetAbstract
{
    protected $_template = 'widgets/tree.twig';

    public function __construct()
    {
        $this->setClass('tree');
        $this->setDataTree('tree');
        $this->setDataCallback('__treeCallback');
        $this->setDataBlurCallback('__treeBlur');
        $this->setDataSelectCallback('__treeSelect');
        $this->setDataActivateCallback('__treeActivate');
        $this->setDataFocusCallback('__treeFocus');
        $this->setDataDropCallback('__treeDrop');
    }
}