<?php
namespace Modules\Widgets\Entities;

class Switcher extends WidgetAbstract
{
    protected $_template = 'widgets/switcher.twig';

    private $_items = [];
    private $_callback;

    public function __construct()
    {
        $id = '__' . md5(microtime());

        $this->setId($id);
        $this->setClass('switcher');
        $this->_callback = $id . '_callback';
    }

    public function setCallback(string $callback) : self
    {
        $this->_callback = $callback;

        return $this;
    }

    public function getCallback() : string
    {
        return $this->_callback;
    }

    public function addItems(array $options = []) : self
    {
        foreach ($options as $option) {
            $this->addItem($option['id'], $option['label'], $option['url'], $option['settings'] ?? []);
        }

        return $this;
    }

    public function addItem(string $id, string $label, string $url, array $settings = []) : self
    {
        $this->_items[] = [
            'id'       => $id,
            'url'      => $url,
            'label'    => $label,
            'settings' => $settings
        ];

        return $this;
    }

    public function getItems() : array
    {
        return $this->_items;
    }
}