<?php
namespace Modules\Widgets\Entities;

class SortableList extends WidgetAbstract
{
    protected $_template = 'widgets/sortable-list.twig';

    public function __construct()
    {
        $this->setId(md5(microtime()));
        $this->setDataWidget('sortable');
        $this->setClass('sortable-list');
    }
}