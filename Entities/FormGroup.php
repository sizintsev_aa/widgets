<?php
namespace Modules\Widgets\Entities;

class FormGroup extends WidgetAbstract
{
    protected $_template = 'widgets/form-group.twig';

    private $title = null;

    public function __construct()
    {
        $this->setClass('form-group');
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }
}