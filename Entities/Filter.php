<?php
namespace Modules\Widgets\Entities;

class Filter extends WidgetAbstract
{
    protected $_template = 'widgets/filter.twig';

    private $_button_name = '';
    private $_scheme = [];

    public function __construct()
    {
        $this->setClass('filter');
    }

    public function setScheme(array $scheme)
    {
        $this->_scheme = $scheme;
        return $this;
    }

    public function getScheme(): array
    {
        return $this->_scheme;
    }

    public function setButtonName(string $button_name) : self
    {
        $this->_button_name = $button_name;

        return $this;
    }

    public function getButtonName() : string
    {
        return $this->_button_name;
    }
}