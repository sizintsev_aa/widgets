<?php
namespace Modules\Widgets\Entities;

class Block extends WidgetAbstract
{
    protected $_content = null;
    protected $_tag     = 'div';

    public function __construct($content = null)
    {
        $this->_content = $content;
    }

    public function setTag(string $tag)
    {
        $this->_tag = $tag;

        return $this;
    }

    public function render() : string
    {
        return '<' . $this->_tag . $this->renderAttributes() . '>' . $this->_content . '</' . $this->_tag . '>';
    }
}