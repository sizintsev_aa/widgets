<?php
namespace Modules\Widgets\Entities;

class Image extends WidgetAbstract
{
    private $_value;
    private $_name;
    private $_id;
    protected $_template = 'widgets/image-uploader.twig';

    public function __construct(string $content = null)
    {
        $this->setClass('image-uploader');
        $this->setValue($content);

        $this->setId('_id_' . md5(microtime()));
    }

    public function setUploadUrl(string $uploadUrl) : self
    {
        $this->setDataAction($uploadUrl);

        return $this;
    }

    public function setRemoveUrl(string $url) : self
    {
        $this->setDataRemove($url);

        return $this;
    }

    public function setId(string $id) : self
    {
        $this->_id = $id;

        return $this;
    }

    public function getId() : string
    {
        return $this->_id;
    }

    public function setName(string $name) : self
    {
        $this->_name = $name;

        return $this;
    }

    public function getName() : string
    {
        return $this->_name;
    }

    public function setValue($value) : self
    {
        $this->_value = $value;

        return $this;
    }

    public function getValue() : string
    {
        return (string) $this->_value;
    }
}