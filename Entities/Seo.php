<?php
namespace Modules\Widgets\Entities;

class Seo extends WidgetAbstract
{
    protected $_template = 'widgets/seo.twig';
    private $_features = [
        'title',
        'h1',
        'keywords',
        'description',
        'og'
    ];

    public function disableFeature(string $feature) : self
    {
        $this->_features = array_diff($this->_features, [$feature]);
        return $this;
    }
    public function disableFeatures(array $features) : self
    {
        $this->_features = array_diff($this->_features, $features);
        return $this;
    }

    public function getFeatures(): array
    {
        return $this->_features;
    }
}