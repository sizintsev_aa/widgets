<?php
namespace Modules\Widgets\Entities;

class Layout extends WidgetAbstract
{
    public function render() : string
    {
        $view = $this->getView();
        return $view->render('widgets::widgets/layout/layout.twig');
    }
}