<?php
namespace Modules\Widgets\Entities;

class Checkbox extends Input
{
    private $_label = null;

    public function __construct($content = null)
    {
        $this->setId('checkbox_' . md5(microtime()));
        $this->setClass('form-checkbox__input');
        $this->setValue($content);
        $this->setType('checkbox');
    }

    public function setLabel(string $label)
    {
        $this->_label = $label;

        return $this;
    }

    public function render() : string
    {
        return '<div class="form-checkbox">'  . parent::render() . '<label for="' . $this->getId() . '" class="form-checkbox__label">' . $this->_label . '</label></div>';
    }
}