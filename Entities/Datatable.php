<?php
namespace Modules\Widgets\Entities;

class Datatable extends WidgetAbstract
{
    protected $_template = 'widgets/datatable.twig';
    protected $_schema   = [];
    protected $_events   = [];
    protected $_actions  = [];

    public function __construct()
    {
        $this->setId(md5(microtime()));
        $this->setDataSearch('true');
        $this->setDataShowRefresh('false');
        $this->setDataShowExport('false');
        $this->setDataShowFooter('false');
        $this->setDataCookie('true');
        $this->setDataPagination('true');
        $this->setDataSidePagination('server');
        $this->setDataQueryParamsType('page');
        $this->setDataPageSize('10');
        $this->setDataPageList('[10, 25, 50, 100, 500]');
    }

    public function setActions(array $actions): self
    {
        $this->_actions = $actions;

        return $this;
    }

    public function getActions(): array
    {
        return $this->_actions;
    }

    public function setEvents($name, string $callback) : self
    {
        if (is_array($name)) {
            $name = array_map(function ($value) {
                return $value . '.bs.table';
            }, $name);

            $name = implode(' ', $name);
        }
        else {
            $name = $name . '.bs.table';
        }

        $this->_events[$name] = $callback;

        return $this;
    }

    public function getEvents(): array
    {
        return $this->_events;
    }

    public function setSchema(array $schema) : self
    {
        $this->_schema = $schema;

        return $this;
    }

    public function getSchema() : array
    {
        if (sizeof($this->_actions) > 0) {
            $this->_schema[] = [
                'title'     => trans('Действия'),
                'field'     => 'id',
                'sortable'  => false,
                'formatter' => 'formatDefaultActions_' . $this->getId(),
                'width'     => 100
            ];
        }

        if ($this->getDataState() == 'checkbox') {
            $this->_events['post-body.bs.table'] = 'redrawCheckbox' . $this->getId();
            $this->_schema[] = [
                'field'     => 'state',
                'checkbox'  => true
            ];
        }

        return $this->_schema;
    }
}