<?php
namespace Modules\Widgets\Entities;

class Input extends WidgetAbstract
{
    protected $_template = 'widgets/input.twig';

    public function __construct($content = null)
    {
        $this->setClass('form-control');
        $this->setValue($content);
    }
}