<?php
namespace Modules\Widgets\Entities;

class Tags extends WidgetAbstract
{
    protected $_template = 'widgets/tags.twig';

    public function __construct($content = null)
    {
        $this->setDataToggle('tags');
        $this->setId('__' . md5(microtime()));
        $this->setClass('form-control');
        $this->setValue($content);
    }
}