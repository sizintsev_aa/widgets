<?php
namespace Modules\Widgets\Entities;

class FileUploader extends WidgetAbstract
{
    private $_value;
    private $_id;
    protected $_template = 'widgets/file-uploader.twig';

    public function __construct($content = null)
    {
        $this->setClass('dropzone');
        $this->setValue($content);
    }

    public function setUploadUrl($uploadUrl)
    {
        $this->setDataAction($uploadUrl);

        return $this;
    }

    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setValue($value)
    {
        $this->_value = $value;

        return $this;
    }
}