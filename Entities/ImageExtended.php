<?php
namespace Modules\Widgets\Entities;
class ImageExtended extends Image
{
    protected $_template = 'widgets/image-uploader-extended.twig';

    public function renderasd() : string
    {
        $view = $this->getView();
        $view ->addGlobal('name', $this->getName());

        return $view->render('widgets/image_extended.twig', [
            'content' => $this->_content
        ]);
    }
}