<?php
/**
 * Use {{ widget('charts').setDataSource(route('index.test')).setStyle('height:500px')|raw }}
 */
namespace Modules\Widgets\Entities;

class Charts extends WidgetAbstract
{
    protected $_template = 'widgets/charts.twig';

    public function __construct()
    {
        $this->setId('id_' . md5(microtime()));
        $this->setDataCharts('chart');
    }
}