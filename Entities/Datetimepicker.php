<?php
namespace Modules\Widgets\Entities;

class Datetimepicker extends WidgetAbstract
{
    protected $_template = 'widgets/datetimepicker.twig';

    public function __construct($content = null)
    {
        $this->setId('__' . md5(microtime()));
        $this->setDataToggle('datetimepicker');
        $this->setClass('form-control');
        $this->setValue($content);
    }
}