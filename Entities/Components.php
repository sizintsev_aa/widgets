<?php
namespace Modules\Widgets\Entities;

class Components extends WidgetAbstract
{
    private $_components = [];
    private $_availableComponents = [
        'heading' => [
            'title' => "Заголовок"
        ],
        'text' => [
            'title' => "Текст"
        ],
        'image' => [
            'title' => "Изображение"
        ],
        'gallery' => [
            'title' => "Галерея"
        ],
        'files' => [
            'title' => "Файлы"
        ],
        'quote' => [
            'title' => "Цитата"
        ],
        'video' => [
            'title' => "Видео"
        ],
        'blocks' => [
            'title' => "Блоки"
        ],
    ];

    public function getAvailableComponents() : array
    {
        return $this->_availableComponents ;
    }

    public function setComponents(array $components = [])
    {
        $this->_components = $components;

        return $this;
    }

    public function render() : string
    {
        $view = $this->getView();
        $view ->addGlobal('name', $this->getName());
        $view ->addGlobal('components', $this->_components);
        $view ->addGlobal('_widget', $this);

        return $view->render('widgets::widgets/components/layout.twig');
    }
}