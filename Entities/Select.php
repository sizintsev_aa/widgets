<?php
namespace Modules\Widgets\Entities;

class Select extends WidgetAbstract
{
    protected $_template = 'widgets/select.twig';
    private $_options    = [];
    private $_selected   = '';

    public function __construct()
    {
        $this->setDataToggle('selectbox');
        $this->setId('__' . md5(microtime()));
        $this->setClass('form-select');
    }

    public function buildFrom($model)
    {
        list ($class, $method) = explode('::', $model);

        try {
            $model = new $class;
            foreach ($model->{$method}() as $item) {
                $this->_options[] = $item;
            }
        }
        catch (\Exception $e) {
        }

        return $this;
    }

    public function getOptions() : array
    {
        return $this->_options;
    }

    public function setSelected($selected) : self
    {
        $this->_selected = $selected;
        return $this;
    }

    public function getSelected()
    {
        return $this->_selected;
    }

    public function addOptions(array $options = []) : self
    {
        foreach ($options as $option) {
            $this->addOption($option['option'], $option['label'], $option['settings'] ?? []);
        }

        return $this;
    }

    public function addOption(string $value, string $label, array $settings = []) : self
    {
        $this->_options[] = [
            'type'     => 'option',
            'value'    => $value,
            'label'    => $label,
            'settings' => $settings
        ];

        return $this;
    }
}