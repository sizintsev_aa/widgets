<?php
/**
 * using
 * {{ widget('reports').setScheme({
        columns : [
        {
        'title' : "колонка 1",
        'items' : [
        {
        'date'    : '2018-02-25',
        'subject' : 'Назначение платежа',
        'amount'  : 2300
        }, {
        'date'    : '2018-02-26',
        'subject' : 'Назначение платежа',
        'amount'  : 2800
        },
        ],
        'total' : 4588
        }
        ]
        }).render()|raw }}
 */
namespace Modules\Widgets\Entities;

class Reports extends WidgetAbstract
{
    protected $_template = 'widgets/reports.twig';
    private $_scheme     = [];

    public function __construct()
    {
        $this->setId('id_' . md5(microtime()));
        $this->setClass('report-columns');
    }

    public function setScheme(array $scheme) : self
    {
        $this->_scheme = $scheme;

        return $this;
    }

    public function getScheme(): array
    {
        return $this->_scheme;
    }
}