<?php
namespace Modules\Widgets\Entities;

use \Modules\Cdn\Entities\CDN;
use Underscore\Types\Strings;

abstract class WidgetAbstract
{
    protected $_i18n     = false;
    protected $_template = 'widgets/div.twig';
    protected $_data     = [];
    protected $_content  = null;

    public function getCdn()
    {
        return new CDN();
    }

    public function enableI18n($state = true)
    {
        $this->_i18n = $state;

        return $this;
    }

    public function isI18n()
    {
        return $this->_i18n;
    }

    public function __call($method, $args)
    {
        if (substr($method, 0, 3) == 'set') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(4);
            $this->_setData($key, (isset($args[0]) ? $args[0] : null));
        }
        elseif (substr($method, 0, 6) == 'append') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(7);
            $this->appendData($key, (isset($args[0]) ? $args[0] : null));
        }
        elseif (substr($method, 0, 5) == 'unset') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(6);
            $this->_unsetData($key);
        }
        elseif (substr($method, 0, 6) == 'remove') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(7);
            $this->_removeData($key, (isset($args[0]) ? $args[0] : null));
        }
        elseif (substr($method, 0, 3) == 'get') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(4);
            return $this->_getData($key);
        }
        elseif (substr($method, 0, 7) == 'prepend') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(8);
            $this->_prependData($key, (isset($args[0]) ? $args[0] : null));
        }
        elseif (substr($method, 0, 3) == 'has') {
            $key = (string) Strings::from($method)->toSnakeCase()->substr(4);
            $this->_hasData($key, (isset($args[0]) ? $args[0] : null));
        }

        return $this;
    }

    protected function _removeData($key, $value = null)
    {
        if (array_key_exists($key, $this->_data)) {
            $this->_data[$key] = str_replace([
                $value
            ], [
                null
            ], $this->_data[$key]);
        }
    }

    protected function _prependData($key, $value = null)
    {
        if (array_key_exists($key, $this->_data)) {
            $this->_data[$key] = $value . ' ' . $this->_data[$key];
        }
    }

    public function appendData($key, $value = null)
    {
        if (array_key_exists($key, $this->_data)) {
            $this->_data[$key] = $this->_data[$key] . ' ' . $value;
        }
    }

    protected function _unsetData($key)
    {
        if (array_key_exists($key, $this->_data)) {
            unset($this->_data[$key]);
        }
    }

    protected function _hasData($key, $value = null) : bool
    {
        if (array_key_exists($key, $this->_data)) {
            return strpos($this->_data[$key], $value) !== false;
        }

        return false;
    }

    protected function _setData($key, $value = null)
    {
        if (is_array($key)) {
            $this->_data = $key;
        }
        else {
            $this->_data[(string )$key] = $value;
        }
    }

    protected  function _getData($key = null)
    {
        if ($key == null)
            return $this->_data;

        if(isset($this->_data[$key])) {
            return $this->_data[$key];
        }
        return null;
    }

    public function setData($key, $value = null)
    {
        if (is_array($key)) {
            $this->_data = $key;
        }
        else {
            $this->_data[(string )$key] = $value;
        }
    }

    public  function getData($key = null)
    {
        if ($key == null)
            return $this->_data;

        if(isset($this->_data[$key])) {
            return $this->_data[$key];
        }
        return null;
    }

    public function renderAttributes(array $attr = [])
    {
        $attributes = null;

        if (sizeof($attr) == 0) {
            $attr = $this->_data;
        }

        foreach ($attr as $attribute => $value) {
            $value = str_replace(" +", " ", $value);
            if (trim($value) != null) {
                if (substr($attribute, 0, 4) == 'data'){
                    $attribute = str_replace('_', '-', $attribute);
                }
                $attributes.= ' ' . $attribute . '="' . trim($value) . '"';
            }
        }

        return $attributes;
    }

    public function getI18n()
    {
        return app()->i18n_list;
    }

    public function getView()
    {
        return app('Twig_Environment');
    }

    public function render() : string
    {
        $twig = $this->getView();

        return $twig->render('widgets::' . $this->_template, [
            '_widget' => $this
        ]);
    }

    public function setContent($content) : self
    {
        $this->_content = $content;

        return $this;
    }

    public function getContent()
    {
        return $this->_content;
    }

    public function __toString()
    {
        return $this->render();
    }
}